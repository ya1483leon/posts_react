import React, { useState } from 'react';
import MyInput from "./UI/input/MyInput";
import MyButton from "./UI/button/MyButton";

function PostForm({create}) {

    const [post, setPost] = useState({title: '', body: ''});


    const addNewPost =(e) => {
        e.preventDefault()
        const newPost = {
            ...post, id: Date.now()
        }
        create(newPost);
        setPost({title: '', body: ''})
    }

    return (
        <form>
            {/* Управляемый инпут */}
            <MyInput type="text"
                value={post.title}
                onChange={event => setPost({...post, title: event.target.value})}
                placeholder="Title"
            />
            {/* Неуправляемый инпут */}
            <MyInput
            value={post.body}
            onChange={event => setPost({...post, body: event.target.value})}
            type="text" 
            placeholder="Description"
            />
            <MyButton onClick={addNewPost} >Create post</MyButton>
        </form>
  )
}

export default PostForm