import React from 'react'
import { useContext } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { AuthContext } from '../context';
import { privateRoutes, publicRoutes } from '../router/router'
import Loader from './UI/Loader/Loader';

function AppRouter() {
    const {isAuth, isLoading} = useContext(AuthContext);

    if (isLoading) {
        return <Loader/>
    }


    return (
        isAuth 
        ?
        <Routes>
            {privateRoutes.map( route => 
                <Route 
                path={route.path}
                exact={route.exact}
                element={<route.element/>} 
                key={route.path}
                />
            )}
            <Route path="/*" element={<Navigate to="/posts" replace />} />
        </Routes>
        :
        <Routes>
            {publicRoutes.map( route => 
                <Route 
                path={route.path}
                exact={route.exact}
                element={<route.element/>} 
                key={route.path}
                />
            )}
            <Route path="/*" element={<Navigate to="/login" replace />} />
        </Routes>
    )
}

export default AppRouter
