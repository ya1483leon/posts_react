import React from 'react'
import { useContext } from 'react'
import { Link } from 'react-router-dom'
import { AuthContext } from '../../../context';
import MyButton from '../button/MyButton';
import '../../../styles/App.css';

function Navbar() {
  const {isAuth, setIsAuth} = useContext(AuthContext);

  const logout = () => {
      setIsAuth(false);
      localStorage.removeItem('auth')
  }

  return (
    <div className="navbar">
        <div className="navbar__links">
                <Link style={{margin: 10}} to="/about">About</Link>
                <Link style={{margin: 10}} to="/posts">Posts</Link>
        </div>
        <MyButton onClick={logout}>
            Logout
        </MyButton>
    </div>
  )
}

export default Navbar